// https://codeberg.org/anhsirk0/some-ts
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const arr = <T = any>(a?: any) =>
  Array.isArray(a) ? a : ([] as Array<T>);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const str = (s?: any) => {
  if (typeof s === "number") return s.toString();
  if (typeof s === "string") return s;
  return "";
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const num = (n?: any) => {
  if (typeof n === "string") {
    const n_ = Number(n);
    return isNaN(n_) ? 0 : n_;
  }
  if (typeof n === "number") return n;
  return 0;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const bool = (b?: any) => {
  if (typeof b === "boolean") return b;
  if (b === true.toString()) return true;
  if (b === false.toString()) return false;
  return Boolean(b);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const obj = <T extends Record<string | number, any>>(o?: any) =>
  typeof o === "object" && !Array.isArray(o) && o !== null
    ? (o as T)
    : ({} as T);
